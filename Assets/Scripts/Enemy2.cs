﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour {

    public float speedx;
    public float speedy;
    public bool isdown;
    private float counter;
	
	void Update ()
    {
        transform.Translate(Vector2.left * speedx * Time.deltaTime);
        if(isdown == true) { 
        transform.Translate(Vector2.up * speedy * Time.deltaTime);
            counter++;
            if(counter >= 15)
            {
                isdown = false;
                counter = 0;
            }
        }
        else if (isdown==false){ 
        transform.Translate(Vector2.down * speedy * Time.deltaTime);
            counter++;
            if(counter >= 15)
            {
                isdown = true;
                counter = 0;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Boundary")
        {
            Dead();
        }
    }

    void Dead()
    {
        Destroy(this.gameObject);
    }
}
